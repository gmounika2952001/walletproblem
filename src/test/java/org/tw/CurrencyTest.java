package org.tw;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.tw.Currency.*;

public class CurrencyTest {
    @Test
    void should_return_true_when_seventy_four_point_eight_five_rupees_equal_to_one_dollar() {
        Currency seventyFourPointEightFiveRupees = rupeesToDollar(74.85);
        Currency oneDollar = dollar(1);
        assertThat(seventyFourPointEightFiveRupees, is(equalTo(oneDollar)));
    }

    @Test
    void should_check_whether_money_is_added_into_wallet() {
        Currency fiftyRupees = rupees(50);
        assertThat(fiftyRupees.moneyAddedToWalletInRupees(), is(equalTo(fiftyRupees)));
    }

    @Test
    void should_return_true_when_we_add_fifty_rupees_and_one_dollar_equal_to_hundred_twenty_four_point_eight_five_rupees() {
        Currency fiftyRupees = rupees(50);
        Currency oneDollar = dollarToRupees(1);
        Currency hundredTwentyFourPointEightFiveRupees = rupees(124.85);

        assertThat(fiftyRupees.add(oneDollar), is(equalTo(hundredTwentyFourPointEightFiveRupees)));
    }

    @Test
    void should_return_true_when_we_add_seventy_four_point_eight_five_rupees_one_dollar_hundred_forty_nine_point_seven_rupees_equal_to_four_dollar() {
        Currency seventyFourPointEightFiveRupees = rupeesToDollar(74.85);
        Currency oneDollar = dollar(1);
        Currency hundredFortyNinePointSevenRupees = rupeesToDollar(149.7);
        Currency fourDollars = dollar(4);

        assertThat(seventyFourPointEightFiveRupees.add1(oneDollar, hundredFortyNinePointSevenRupees), is(equalTo(fourDollars)));
    }


}
